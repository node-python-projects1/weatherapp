var express = require('express')

var app = express()

var cors = require('cors')

app.use(cors())

var port = process.env.port || 3000

app.use(express.json())

var weatherData = require('../utils/weatherData')

app.get('/', (req, res) => {
  const address = req.query.address
  weatherData(address, (error, { temperature, description, cityName }) => {
    console.log(temperature, description, cityName)
    if (error) {
      return res.send({
        error,
      })
    }
    res.send(
      `The temperature at  ${cityName}   is  ${temperature},  found with  ${description}`,
    )
  })
})

app.listen(port, (err) => {
  if (err) {
    console.log(err)
  } else {
    console.log('Server is up and running at port: ', port)
  }
})
